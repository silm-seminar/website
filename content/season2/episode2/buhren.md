+++
title = "Taking apart AMD's root-of-trust: Reverse engineering the AMD Secure Processor"
date = 2021-04-23T12:00:00Z

[extra]
speaker_name = "Robert Buhren"
speaker_institution = "TU Berlin"
links = ["bh_talk", "_36c3_talk", "ccs19"]

bh_talk_link = "https://www.blackhat.com/us-20/briefings/schedule/index.html#all-you-ever-wanted-to-know-about-the-amd-platform-security-processor-and-were-afraid-to-emulate---inside-a-deeply-embedded-security-processor-20106"
bh_talk_label = "BlackHat talk"

_36c3_talk_link = "https://media.ccc.de/v/36c3-10942-uncover_understand_own_-_regaining_control_over_your_amd_cpu"
_36c3_talk_label = "36c3 talk"
_36c3_talk_type = "video"

ccs19_link = "https://dl.acm.org/doi/pdf/10.1145/3319535.3354216"
ccs19_label = "Insecure Until Proven Updated:Analyzing AMD SEV’s Remote Attestation, CCS19"
ccs19_type = "file-pdf"

+++
Modern AMD CPUs contain a dedicated security co-processor, the AMD Secure Processor (AMD-SP). The AMD-SP, formerly known as Platform Security Processor (PSP), constitutes the root-of-trust of the whole AMD SOC and is part of the AMD die. It is responsible for hosting firmware components related to security features such as AMD's Secure Encrypted Virtualization technology or firmware TPMs found in AMD desktop systems, as well as for initializing the AMD SOC. In this talk, I present an overview of our reverse-engineering efforts of the AMD-SP. I introduce the tools we developed to tinker with the SP's firmware components, including our emulator for the SP that allows you to analyze the SP's runtime behavior on your laptop! Furthermore, I present firmware and bootrom issues that allow an attacker to take full control over the AMD-SP.