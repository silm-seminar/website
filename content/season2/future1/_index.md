+++
title = "Future episode on formal hardware"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 5

[extra]
live = false
date = 2021-06-11T12:00:00Z
+++