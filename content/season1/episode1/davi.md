+++
title = "Memory Corruption Attacks in the Context of Trusted Execution Environments"
date = 2019-11-08T09:00:00Z

[extra]
speaker_name = "Lucas Davi"
speaker_institution = "University of Duisburg-Essen"
inria_video_link = "https://videos-rennes.inria.fr/video/BksfYbigI"
+++
ARM TrustZone and Intel Software Guard Extensions (SGX) offer hardware-assisted trusted execution environments (TEEs) to enable strong isolation of security-critical code and data. They also allow systems to perform remote attestation, where a device challenges another device to report its current state. In this talk, we elaborate on remote attestation schemes that do not only attest static properties, but also cover run-time control-flow behavior of applications based on ARM TrustZone. While TEEs enable secure attestation of control-flow behavior, memory corruption attacks (e.g., return-oriented programming) inside TEEs can undermine remote attestation schemes. This talk will elaborate on memory corruption attacks for the use-case of SGX and how we can develop analysis approaches to detect vulnerable TEE code.