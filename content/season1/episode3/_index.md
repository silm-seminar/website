+++
title = "Episode 3"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 3

[extra]
live = false
date = 2020-02-18T08:00:00Z
+++