+++
title = "Software side-channel attacks and fault attacks on ARM devices"
date = 2020-02-18T08:15:00Z

[extra]
speaker_name = "Clémentine Maurice"
speaker_institution = "CRNS/EMSEC"
inria_video_link = "https://videos-rennes.inria.fr/video/BJ8vZno3I"
duration = 30
+++
In the last years, mobile devices and smartphones have become the most important personal computing platform.
Besides phone calls and managing the personal address book, they are also used to approve bank transfers and digitally sign official documents, thus storing very sensitive secrets. 
However, the large majority of research work in software side-channel attacks and fault attacks is targeting x86 CPUs (such as laptops and servers). 
In this presentation, we will present side-channel attacks on microarchitecture and DRAM fault attacks on ARM devices, and we will highlight the main particulars of the platform and necessary adaptations of the attacks and defences.
