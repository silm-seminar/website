+++
title = "Embedded security around RISC-V cores"
date = 2020-01-17T10:00:00Z

[extra]
speaker_name = "Yann Loisel"
speaker_institution = "SiFive"
+++
The security needs increase a lot in our connected world. Beyond the perception the software can satisfy the security requirements, it’s obvious that the best solutions could only be a combination of hardware and software mechanisms.
The huge activity around the RISC-V cores these last years is not only a buzz or for an unjustified reason. On the contrary, this ecosystem is more than only a new ISA: it is the ideal playground for applying the best practices in embedded security.
We will see in this talk what the RISC-V foundation and its academic and industrial members propose for making RISC-V a synonym of security at the core, in the software and at the system level.